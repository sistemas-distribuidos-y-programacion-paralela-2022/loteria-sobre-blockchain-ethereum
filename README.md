# Loteria sobre Blockchain Ethereum

Para el tp final de la asignatura.

## Smart Contract de la loteria subido

Para probarlo de puede utilizar el IDE online REMIX, que cuenta con redes de prueba y cuentas con ETH suficiente.

La idea es deployar el Smart Contract en la red de prueba Rinkeby de Ethereum, pudiendo solicitar ETH de prueba. Luego conectarse desde apps desplegada en la nube(aws) mediante Web3j para simular al admin y a los jugadores de la loteria.

## Link al documento formal con los diagramas

https://internal-iberis-42c.notion.site/TP-FINAL-Loteria-Blockchain-c69acd78f593435b80d6be213340c189
