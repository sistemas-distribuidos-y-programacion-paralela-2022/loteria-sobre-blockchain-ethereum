//SPDX-License-Identifier: GPL-3.0
//licencia para evitar warnings/errores


pragma solidity >=0.5.0 <0.9.0; //Version del compilador
 
 //El codigo del contrato podria ser publico para inpirar confianza en los jugadores 

contract Loteria{
    
    // Declaracion de las state vars
    address payable[] public jugadores; // Array dinamico con las direcciones de los jugadores
    //Debe ser payable para poder enviar ETH al jugador ganador
    
    // Direccion del admin de la Loteria 
    address public admin; 
    
    // Declaracion del constructor
    constructor(){
        // Se inicializa el admin en el constructor con la direccion de quien deploya el contrato
        admin = msg.sender; 
    }
    
    // Declaracion de la funcion necesaria para poder recibir ETH
    receive () payable external{
        // Se valida que cada jugador envie exactamente 0.1 ETH 
        // siempre las validaciones al principio para evitar el consumo inecesario de gas de las operaciones
        require(msg.value == 0.1 ether);
        // Se agrega al jugador al array de jugadores
        jugadores.push(payable(msg.sender));
    }
    
    // Retorna el balance del contrato en formato wei (la unidad mas pequenia de ETH)
    function getBalance() public view returns(uint){
        // Se valida que solo el admin pueda invocar esta funcion
        require(msg.sender == admin);
        // Se devuelve el balance de la direccion de esta instancia del contrato
        return address(this).balance;
    }
    
    // Esta funcion devuelve un entero grande en funcion de tres parametros:
    // - la dificultad del bloque
    // - El timestamp del bloque
    // - La cantidad de jugadores que se subscribieron al contrato
    // Por lo tanto es un numero pseudo aleatorio porque un atacante podria intentar inferir los valores
    // A los efectos del TP de Sistemas Distribuidos pienso que el numero generado es suficiente 
    function random() internal view returns(uint){
       return uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp, jugadores.length)));
    }
    
    
    // Funcion que elige un ganador
    function elegirGanador() public{
        // Se valida que solo el admin pueda ejecutar la funcion
        // Se valida que la cantidad de jugadores sea al menos 3
        // siempre las validaciones al principio para evitar el consumo inecesario de gas de las operaciones
        require(msg.sender == admin);
        require (jugadores.length >= 3);
        
        uint r = random();
        address payable ganador;
        
        // Se genera un indice aleatorio para elegir a un ganador
        uint indice = r % jugadores.length;
    
        ganador = jugadores[indice]; // Se elige un ganador
        
        // Se transfiere todo el balance del contrato al ganador
        ganador.transfer(getBalance());
        
        // Se reinicia la loteria para las proximas rondas
        jugadores = new address payable[](0);
    }
 
}
